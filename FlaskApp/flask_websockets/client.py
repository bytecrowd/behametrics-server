import sys
sys.path.append('/var/www/behametrics-server/FlaskApp/flask_websockets/behametrics-ml')
from Model import Model
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import os

sensors = ['acc', 'move']


class Client:

    def __init__(self, sid, socketio, id_client, configs):
        self.id_client = id_client
        self.sid = sid
        self.socketio = socketio
        self.model = Model(configs['DATASETS_FOLDER'], configs['MODELS_FOLDER'], self.id_client, sensors)
        self.observer = Observer()

    def run_learn(self):
        print("SID " + self.sid)
        self.model.train()        
        self.socketio.emit('learn', "Learned, Start authentication...", room=self.sid)
        self.register_dir(self.model.path_to_raw_data)

    def run_auth(self):
        value = self.model.predict()
        print(value)
        if value is not None:
            self.socketio.emit('authenticate', value, room=self.sid)

    def register_dir(self, path):
        os.system("rm -r " + self.model.path_to_raw_data + "/*")
        event_handler = MyHandler(self)
        print("Registering" + path)
        self.observer.schedule(event_handler, path, recursive=True)
        self.observer.start()


def make_client(sid, socketio, id_client, configs):
    client = Client(sid, socketio, id_client, configs)
    return client


class MyHandler(FileSystemEventHandler):

    def __init__(self, client):
        self.client = client

    def on_modified(self, event): 
        if os.listdir(self.client.model.path_to_raw_data)!=[]:
            self.client.run_auth()

    #def on_created(self, event):
     #   self.client.run_auth()
