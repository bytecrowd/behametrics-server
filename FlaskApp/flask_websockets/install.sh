python3 -m pip install --user --upgrade pip
python3 -m pip install --user virtualenv
python3 -m virtualenv env
source env/bin/activate
pip3 install flask-socketio
pip3 install sklearn
pip3 install numpy
pip3 install scipy
pip3 install pandas
pip3 install watchdog
pip3 install keras
pip3 install tensorflow==1.5.0
