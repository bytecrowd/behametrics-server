from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
from client import make_client
import threading
from upload import upload_file
import shutil
import os
#from path import Path

# initialize Flask
app = Flask(__name__)
app.config.from_pyfile('config.py')
configs = app.config
socketio = SocketIO(app, async_mode='threading')

client_list = {}


@app.route('/', methods=['POST'])
def index():
    return upload_file(request)


@socketio.on('connectSocket')
def connect(msg):
    if msg in client_list:
        print(msg + ' exists!')
        client_list[msg].sid = request.sid
        return 
    print('Connected:' + msg)
    sid = request.sid
    client = make_client(sid=sid, socketio=socketio, id_client=msg, configs=configs)
    client_list[client.id_client] = client


@socketio.on('authenticate')
def auth(msg):    
    client = client_list[msg]
    print("learn")
    if client:
        #if os.path.exists(client.model.path_to_model):
        if os.listdir(client.model.path_to_model) == []:
            threading.Thread(target=client.run_learn, args=()).start()
        else:
            client.register_dir(client.model.path_to_raw_data)


@socketio.on('learn')
def learn(msg):
    client = client_list[msg]
    if client and os.listdir(client.model.path_to_model) != []:
        shutil.rmtree(client.model.path_to_model)
        client.observer.stop()
        os.mkdir(client.model.path_to_model)


@socketio.on('disconnect')
def disconnect():
    x = False 
    for client in client_list:
        if client_list[client].sid == request.sid:
            x = True
            rem = client
    
    if x:
        client_list[rem].observer.stop()
        client_list.pop(rem, None)
    print(client_list)
    print('Disconnected')


if __name__ == '__main__':
    socketio.run(app, debug=True)
