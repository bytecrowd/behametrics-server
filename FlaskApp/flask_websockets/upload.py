import os
import os.path
from werkzeug.utils import secure_filename
from pathlib import Path

ALLOWED_EXTENSIONS = set(['csv'])
target_dir = '/var/www/html/logger/datasets/'


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def upload_file(request):
    subdir = ""    
    if request.method == 'POST':
        file = request.files['fileToUpload']
        if file.filename == '':
            return '', 400
    if file and allowed_file(file.filename): 
        target_mac = request.headers.get('uuid')          
        dirname = request.headers.get('dirname').split('/')
        last = len(dirname)

        for i in range(0, last-1):
            subdir += '/'+ dirname[i]
                   
        subdir = target_dir +"/"+ target_mac + "/" + subdir #dirname[0] +"/"+target_mac #+ "/" + subdir
        subdir = Path(subdir)
        target_file = str(subdir) + '/' + dirname[last-1]

        if not subdir.is_dir():
            os.makedirs(str(subdir), 0o777)

        if os.path.exists(target_file):
            os.unlink(target_file)

        secure_filename(target_file)
        file.save(target_file)
        return '', 200
    return '', 400

