## User manual

### Logger and Authentication

#### Requirements

Install:
* python3
* pip3

#### Installation

For installation of Behametrics-server run following command:
 
```
cd to directory where you want to clone app
git clone https://gitlab.com/bytecrowd/behametrics-server.git
cd behametrics-server/FlaskApp/flask_websockets/
git clone https://gitlab.com/bytecrowd/behametrics-ml.git
./install.sh 
```

#### Running

For running Behametrics-server run following command 
```
cd to directory where you clone project
cd behametrics-server/FlaskApp/flask_websockets/
./run.sh <ip address> <port>
``` 

If not running, run following command:
```
source env/bin/activate
```


