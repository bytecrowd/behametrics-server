<?php
$target_dir = "datasets/";	
$target_mac = $_SERVER['HTTP_UUID'];
$file = $_FILES["fileToUpload"]["name"];
$dirName = explode('/',$_SERVER['HTTP_DIRNAME']);
$last = count($dirName) - 1;
$target_subdir = "";

for($i=1;$i<$last; $i++)
{
	$target_subdir .= '/'.$dirName[$i];
}

$target_subdir = $target_dir ."/".$dirName[0] ."/". $target_mac. "/" . $target_subdir;
$target_file = $target_subdir."/". $dirName[$last];//basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$idList = array("bytecrowd","testid");

//if using HTTPS check for id
if(isset($_SERVER["HTTPS"]))
{	
	if(!in_array($_SERVER['HTTP_CLIENTID'],$idList))
	{
		$uploadOk = 0;
		error_log("Wrong client ID",0,"/var/log/apache2/error.log");
	}
}

if($_FILES["fileToUpload"]["error"] > 0) 
{
	http_response_code(403);
	error_log("Error in the file uploaded",0,"/var/log/apache2/error.log");	
	echo "Error : ".$_FILES["fileToUpload"]["error"]."";
	$uploadOk = 0;

}
// Check if subdir name was specified
/*if(empty($_SERVER['HTTP_DIRNAME']))
{	
	error_log("dirName is empty",0,"/var/log/apache2/error.log");

	http_response_code(400);
	
	echo "dirName is empty.";
	$uploadOk = 0;
}
else
{*/
if (!file_exists($target_subdir)) 
{
	mkdir($target_subdir, 0777, true);
}
//}
//Check if file exists
if (file_exists($target_file)) {
	unlink($target_file);
}
// Check file size , max 1MB
if ($_FILES["fileToUpload"]["size"] > (1 << 20)) {
    echo "Sorry, your file is too large.";
    error_log("The uploaded file is too large",0,"/var/log/apache2/error.log");

    $uploadOk = 0;
    http_response_code(400);
}
// Allow certain file formats
if($imageFileType != "csv") {
    echo "Sorry, only csv is allowed, current data format is ".$imageFileType;
    error_log("The uploaded file is not csv",0,"/var/log/apache2/error.log");

    $uploadOk = 0;
    http_response_code(400);
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    error_log("File not uploaded",0,"/var/log/apache2/error.log");
    http_response_code(400);
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file stored in tmp storage
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
	    http_response_code(200);
	    echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
	error_log("There was an error uploading",0,"/var/log/apache2/error.log");
	http_response_code(400);    
        echo "Sorry, there was an error uploading your file.";
    }
}
?>
